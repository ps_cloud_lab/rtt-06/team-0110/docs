# AWS Notes

## Angela
Mkdir creates directories. \
(.) takes you back home. \
git switch (branch) allows you to switch out the branch you are in. 

The first layer of security prevention measures focuses on protecting the network. This protection can be achieved through network discovery hardening and network security architecture hardening. This slide lists some examples of actions for each type of hardening.

The goal of network discovery hardening is to prevent an attacker from discovering, exploring, or mapping the network. Unfortunately, the same tools that network administrators use to explore or map networks can compromise network security if malicious entities are allowed to use them. (Examples of these tools include ping and Nmap.) As a result, network discovery tools should be blocked because they can be used to exploit network systems.

Another effective network hardening action is to close network ports that are not used because they present open doors for attackers to come in. In addition, you can use your asset inventory list to identify and enforce the list of devices that are allowed on your network. If a device that is not on the allow list appears on your network, you can immediately investigate and take proper action.

Hardening the security architecture of your network is another important prevention measure. For example, you can use firewalls in your network topology to protect resources such as web servers and database servers. A firewall permits only a certain type of traffic (based on protocol and source IP address) to come into the protected resource. You can also use segments to break down the network so that critical resources run in their own segment, which gives them additional protection. 

The OSI model provides another example of how security can be implemented in layers. Each of the model’s layers provides an opportunity to implement a security solution:
- Physical layer: Network devices and equipment are protected from physical access to keep intruders out.
- Data link layer: Filters are applied to network switches help prevent attacks based on media access control (MAC) addresses.
- Network and transport layers: Implementing firewalls and access control lists (ACLs) helps to mitigate unauthorized access to internal systems.
- Session and presentation layers: By using authentication and encryption methods, you can prevent unauthorized data accesses.
- Application layer: Solutions, such as virus scanners and an IDS, help protect applications.
Protect each layer separately to make it as difficult as possible for an outside party to breach your defenses and gain access to your resources


## Lauchland
### Cloud Acquisition Fundamentals
#### Security and Compliance (How they work together):
Just like there is Governance of an organization there is also a need for Compliance to the requires set in place by the customer's need. Service provisioning cuppled with the efficiency of creation, reliability of instance's, as well as scalability all work together in supporting the growth of the organization.   

I will show a brief overview as to how Security and Compliance work together harmoniously in the cloud environment. U.S. Health Dept's. require certain types of organizational committees to assure the framework is stable and trackable. For example the United States Health Insurance Portability and Accountability Act of 1996 (HIPPA) and the Health Information Technology for Economic and Clinical Health Act (HITECH) both are needed in the field of Government Health programmimg. The security groups within an organizatin support compliancy by restricting access to private and personal information. We see security breaches as an issue in Comliance.

### Lauchland 3/21/2023 - Daily Commit
I made adjustments to my learning style this week. I would consider myself a Logical/analytical learner. This past week I have released my heart into accepting the kinesthetic learning style. The kinesthetic learner would rather perform physical activity to learn something, as an active participant, instead of passively listening to a lecture or watching a demonstration.

### Lauchland Watkins 3/24/2023 - Daily Commit
When you create a presigned URL for your object, you must provide your security credentials and then specify a bucket name, an object key, an HTTP method (GET to download the object), and an expiration date and time. The presigned URLs are valid only for the specified duration. If you created a presigned URL using a temporary token, then the URL expires when the token expires, even if the URL was created with a later expiration time.

AWS has 6 pillars.

### AWS Well Architected Framework
Security Pillar
Incident Response-
Topic- Simulate
Post-incident debrief: This session allows teams to share learnings and increase the overall effectiveness of the organization’s incident response plan. Here the teams should review handling of the incident in detail, document lessons learned, update runbooks based on learnings, and determine if new risk assessments are required

### 3/30/2023 Thursday Commit
Today I ran into a snag for the first time in a long time. I was working within a group with additional cohorts and could not focus on the work in front of me as we all took turns sharing screens and knowledge. I learned that AWS Command Line Interface (CLI) is robust and ye it is still so easy to navigate.
I found it interesting that the CSS file has so my other things within.
I have no questions at this moment.

## Tremaine
Heres my commit 3/17/23
Git pull- retrieves info from your current git and updates in your CLI

this is new commit, i started work this week and also learning a new GUI app for git in order to see the structure of it and order

## Ali
### Serverless Compute
EC2 is one of the most popular and basic services, but it requires you to manage your own server. AWS has a multiple services which don't require you to manage a server. This allows you to focus entirely on the the business code instead of worrying about managing and maintaining a server. If your service also doesn't need to be constantly running or monitoring for activity, you are wasting money by paying for an EC2 instance which is charged by the second for the time it is up, not the time that your workload is actually running. 

#### Lambda
AWS Lambda is one of the more popular and versatile serverless services. Lambda will run some code for you in response to an event. The number of events that can trigger Lambda are large, including HTTP requests via Amazon API Gateway, or modifications or updates to other AWS managed services like S3 or DynamoDB. Since AWS will automatically handle scaling the infrastructure for you, you do not need to worry about the number of Lambda invocations and can focus on just the code for your service instead of dealing with the infrastructure. the The free tier of AWS allows for 1 million requests of Lambda per month, with a limit of 400,000 GB-seconds of compute (a GB-second is a unit of computing that multiplies time spent and memory used to measure how computationally intensive a task is).

### Networking

#### Network ACLs (Access Control Lists), Security Groups and Firewalls
Both network Access Control Lists (ACLs) and security groups serve simimilar functions. They are like virtual firewalls that allow to control what sort of traffic is allowed to reach your VPC and resources inside it. However, there are some differences between the two that might affect which one you want to use. The first difference is in the default behavior of these. The default ACL will allow all all traffic. Security groups are the opposite; by default all traffic will be blocked. There are also differences in what resources they apply to. ACLs work at the subnet level. If you want to apply the same rules to multiple instances in a subnet, it might be easier to use an ACL. Security groups are attached to specific resources such as an EC2 instance. Security groups are also stateful, that is they remember information or state about the packets that are traveling through them. If some traffic is allowed into a security group, then it is also automatically allowed out. In contrast, ACLs are stateless and they don't remember which packets were allowed in. If you want to traffic that that was allowed in to leave, you must also specify that it is allowed out.

In addition to network ACLs and security groups, AWS also has a Network Firewall and a Web Application Firewall (WAF). These provide similar features to security groups and ACLs but have a few differences. One is that they work at the level of the entire VPC, unlike ACLs and security groups which work at the subnet and instance level respectively. Another difference is that the Network Firewall is more flexible and can work across more levels of the stack, compared to ACLs and security groups which work at the IP/transport layer. AWS Firewall  filter based on application level protocols such as HTTP. 

### Storage 
#### Instance store vs EBS (Elastic Block Store): 

Both instance store and EBS can be attached to EC2 instances, but instance stores are temporary and are lost when the EC2 instance is stopped or terminated. You should use instance stores for temporary scratch work, or as a cache for data that is stored more permanently somewhere else. While the lack of persistence is a big disadvantage, instance stores are directly connected to your EC2 instances's hardware, so they should be very fast.

EBS can also be associated to EC2 instances. However, they are more flexible and your data persists after your instance is stopped. EBS stores can also be moved and attached to different EC2 instances. They also support useful features like encryption and snapshotting for backups out of the box. Since EBS stores aren't usually directly attached to the EC2 hardware, they have to communicate over the network, which might increase latency compared to instance stores.
